<?php
function iut_wp_enqueue_scripts() {
	$parenthandle = 'twentynineteen-style';
	$theme        = wp_get_theme();

	wp_enqueue_style( 
    	$parenthandle,
		get_template_directory_uri() . '/style.css', 
		array(),
		$theme->parent()->get( 'Version' )
	);


	wp_enqueue_style( 
    	'iut-style',
		get_stylesheet_uri(),
		array( $parenthandle ),
		$theme->get( 'Version' )
	);
}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );

function iut_register_post_type_project()
{
  register_post_type(
    'Recipe',
    array(
      'labels' => array(
        'name' => __('Recipes'),
        'singular_name' => __('Infos complémentaires'),
      ),
      'public' 						=> true,
      'publicly_queryable' => true,
      'menu_icon' => 'dashicons-carrot',
      'has_archive' => 'Recettes',
      'herarchical' => false,
      'show_in_rest' 			=> true, 	// Nécessaire pour le fonctionnement avec Gutemberg
      'supports' => array('title', 'editor'),
      'rewrite' => array('slug' => 'recette'),
    )
  );
}

add_action('init', 'iut_register_post_type_project', 10);


/*** Création metabox ***/

function iut_add_meta_boxes_project($post) {
	add_meta_box(
		'iut_mbox_project',                 	// Unique ID
		'Infos complémentaires du projet',      // Box title
		'iut_mbox_project_content',  			// Content callback, must be of type callable
		'recipe'                            	// Post type
	);
}

add_action('add_meta_boxes', 'iut_add_meta_boxes_project');

function iut_mbox_project_content($post) {
	// Get meta value
	
	$iut_ingredients = get_post_meta(
		$post->ID,
		'iut-textarea',
		true
	);


	echo '
	<p>
		<label for="iut-mail">Ingredients : </label>
		<input id="iut-textarea" name="iut-textarea" value="'. $iut_ingredients .'"/>
	</p>
	';
}

// Save post meta

function iut_save_post($post_id) {
	if (isset($_POST['iut-textarea']) && !empty($_POST['iut-textarea'])) {
		update_post_meta(
			$post_id,
			'iut-textarea',
			sanitize_text_field($_POST['iut-textarea'])
		);
	}
}

add_action('save_post', 'iut_save_post');

?>